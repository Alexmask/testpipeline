package JimmyAlexisNassym.RestAPIProject;

import JimmyAlexisNassym.RestAPIProject.Models.User;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserTest {

    @Test
    void contextLoads() {}

    @Test
    void setId() {
        //Given
        User u = new User();

        //when
        u.setId(1);
        assertEquals(u.getId(), 1);
    }

    @Test
    void setName() {
        //Given
        User u = new User();

        //When
        u.setName("oui");
        assertEquals(u.getName(), "oui");
    }

    @Test
    void testFirstName() {
        //Given
        User u = new User();

        //When
        u.setFirstName("oui");
        assertEquals(u.getFirstName(), "oui");
    }
}
