package JimmyAlexisNassym.RestAPIProject.Models;

public class User {
    public int Id;

    public String Name;

    public String FirstName;

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getName() {
        return Name;
    }

    @Override
    public String toString() {
        return "User{" +
                "Id=" + Id +
                ", Nam='" + Name + '\'' +
                '}';
    }
}
