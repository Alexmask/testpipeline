package JimmyAlexisNassym.RestAPIProject.Controllers;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
        import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin(origins = "*")
public class MyController {

    @GetMapping("/index")
    public String index() {
        return "Greetings from Spring Boot!";
    }

}